import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

import { Gang } from '../../shared/gang';
import { CharacterAddFormComponent } from 'src/app/forms/character-add-form/character-add-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Character, CharacterShape } from 'src/app/shared/character';

@Component({
  selector: 'app-gang-roster',
  templateUrl: './gang-roster.component.html',
  styleUrls: ['./gang-roster.component.scss']
})
export class GangRosterComponent implements OnInit {
  gang?: Gang;
  characters?: Character[];
  gangId: any;
  playerId: any;

  constructor(public apiService: ApiService, private route: ActivatedRoute, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.playerId = params.playerId;
      this.gangId = params.gangId;
      this.apiService.getGang(this.playerId, this.gangId).subscribe((data) => {
        this.gang = new Gang(data);
      }, (error) => {
        console.log(error);
      });
      this.apiService.getCharacters(this.playerId, this.gangId).subscribe((data) => {
        this.characters = data.map((character: CharacterShape) => {
          return new Character(character);
        });
      },(error) => {
          console.log(error);
        });
    });
  }

  addCharacter(): void {
    const modal = this.modalService.open(CharacterAddFormComponent, { size: 'xl ' });
    modal.componentInstance.gangId = this.gangId;
  }
}
