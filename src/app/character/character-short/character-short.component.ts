import { Component, Input, OnInit } from '@angular/core';
import { Gang } from 'src/app/shared/gang';
import { Character } from 'src/app/shared/character';

@Component({
  selector: 'app-character-short',
  templateUrl: './character-short.component.html',
  styleUrls: ['./character-short.component.scss']
})
export class CharacterShortComponent implements OnInit {
  @Input() gang?: Gang;
  @Input() character?: Character;

  constructor() {
  }

  ngOnInit(): void {
  }

}
